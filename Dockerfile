FROM gcr.io/google.com/cloudsdktool/cloud-sdk:alpine
MAINTAINER DGC
ENV TERRAGRUNT_VERSION=0.23.20
ENV TERRAGRUNT_TFPATH=/bin/terraform
ENV TERRAFORM_VERSION=0.13.2
ENV TERRAFORM_FILENAME=terraform_${TERRAFORM_VERSION}_linux_amd64.zip
ENV TERRAFORM_URL=https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/${TERRAFORM_FILENAME}
ENV TERRAFORM_SHA256SUM=6c1c6440c5cb199e85926aea65773450564f501fddcd7876f453ba95b45ba746
RUN apk add --update git bash wget curl jq python py-pip
RUN wget -q ${TERRAFORM_URL}
RUN unzip ${TERRAFORM_FILENAME} -d /bin
RUN rm -f ${TERRAFORM_FILENAME}
RUN curl -sL https://github.com/gruntwork-io/terragrunt/releases/download/v$TERRAGRUNT_VERSION/terragrunt_linux_386 \
  -o /bin/terragrunt && chmod +x /bin/terragrunt
RUN gcloud components install kubectl