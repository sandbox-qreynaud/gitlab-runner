# GITLAB RUNNER
This repository gives Terraform modules to help you to deploy a gitlab runner 
in GCP.

You can also find an exemple of how to deploy resources with Terraform through a gitlab-ci pipeline.

## Table of contents
* [Automation Resources](#automation-resources)
* [Docker image](#docker-image)
* [Gitlab CI](#gitlab-ci)

## Automation Resources
To use a gitlab runner un GCP a number of required resources are required.
In the `./automation_resources` folder you can find all necessary Terraform to deploy a 
gitlab runner instance in a dedicated prodject with right roles.
For more information consult the [automation module documentation](./automation_resources/modules/automation/README.md).

## Docker image
To execute jobs described in the gitlab pipeline, the Gitlab runner use Docker environment. 
The file `./Dockerfile` contain all required dependences to execute Terraform, Terragrunt and kubctl jobs.
Use this file to build the Docker image and push it to a registry.

## Gitlab CI
In the file `.gitlab-ci.yml` you can find an example of a gitlab-ci pipile. 
This example deploy Terraform script which in the root directory.
The `main.tf` file contain the description of a Google Compute Instance. The gitlab runne will deploy 
automatically this resource on each modification pushed on the master branch.
