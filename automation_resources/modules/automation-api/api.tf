###       ENABLE API         ###
################################

resource "google_project_service" "kms" {
  project = var.project_id
  service = "cloudkms.googleapis.com"
}

resource "google_project_service" "secret-manager" {
  project = var.project_id
  service = "secretmanager.googleapis.com"
}

resource "google_project_service" "service-networking" {
  project = var.project_id
  service = "servicenetworking.googleapis.com"
}

resource "google_project_service" "bq-data-transfer" {
  project = var.project_id
  service = "bigquerydatatransfer.googleapis.com"
}