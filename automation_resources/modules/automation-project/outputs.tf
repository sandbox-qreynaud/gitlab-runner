output "automation_project_id" {
    value       = google_project.project_automation.project_id
    dexcription = "ID of the automation project which is [project_name]-[random_id]"
}
