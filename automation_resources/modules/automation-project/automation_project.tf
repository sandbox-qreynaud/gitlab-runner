/*******************************************
  Create Project Automations
*******************************************/
resource "random_id" "number_random" {
  byte_length = 2
}
resource "google_project" "project_automation" {
  name                = var.auto_project_name
  project_id          = "ss-automation-cicd-${random_id.number_random.dec}"
  billing_account     = var.billing_id
  org_id              = var.org_id
  auto_create_network = true
}

/*******************************************
  Activate required API's in the project
*******************************************/
resource "google_project_service" "compute-api" {
  project = google_project.project_automation.project_id
  service = "compute.googleapis.com"
}

resource "google_project_service" "logging-api" {
  project = google_project.project_automations_foundations.project_id
  service = "logging.googleapis.com"
}

resource "google_project_service" "ressource-manager-api" {
  project = google_project.project_automations_foundations.project_id
  service = "cloudresourcemanager.googleapis.com"
}
