/********************************************
* AUTOMATION PROJECT
********************************************/

variable "auto_project_name" {
    type        = string
    default     = "ss-automation-cicd"
    description = "Name of the project which host the gitlab runner. The project_id is also defined from this name followed by a random id"
}

variable "billing_id" {
    type        = string
    description = "ID of the billing account to link to the automation project"
}

variable "org_id" {
    type        = string
    description = "organization ID of the automation project"
}