# GitLab Runner automation project
The module automation setup un new project with a GCE instance with execute Gitlab runner.

The following resources are deployed:
* Project
* API's activation
* Service account 
* GCE instance

### Inputs

| Name                 | Description                                                   |  Type         | Default                       | Required |
| -------------------- | ------------------------------------------------------------- | ------------- | ----------------------------- | -------- |
| auto_project_name    | Name of the automation project                                | string        | ss-automation-cicd            | no       |
| billing_id           | ID of the biiling account to link to the automation project   | string        | n/a                           | yes      |
| org_id               | The parent organization ID of the automation project          | string        | n/a                           | yes      |
| region               | Region where is deployed the Gitlab runner instance           | string        | europe-west1                  | no       |
| zone                 | Zone where is deployed the GitLab runner instance             | string        | europe-west1-b                | no       |
| machine_type         | Machine type of the Gitlab Runner                             | string        | n1-standard-1                 | no       |
| service_account_id   | ID used in the generated service account email address        | string        | gcp_sa_shared_services_runner | no       |
| service_account_name | Name of the Gitlab runner service account                     | string        | gitlab-runner                 | no       |


### Outputs

| Name                      | Description                                                      |  Type         |
| ------------------------- | ---------------------------------------------------------------- | ------------- |
| automation_project_id     | ID of the automation project which is [project_name]-[random_id] |   string      |
| sa_automation_email       | Generated email address of the Gitlab runner service account     |   string      |
| runner_instance_self_link | Self link of the gitlab runner instance                          |   string      |

