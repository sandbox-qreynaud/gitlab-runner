/***************************************************
  Service Account creation for gitlab-runner
****************************************************/
resource "google_service_account" "service_account_git" {
  account_id   = var.service_account_id
  display_name = var.service_account_name
  project      = google_project.project_automation.project_id
}
/***************************************************
  Service Account role for gitlab-runner 
****************************************************/
resource "google_organization_iam_member" "service_account_git_binding_owner" {
  org_id = var.organizations
  role   = "roles/owner"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_folder_admin" {
  org_id = var.organizations
  role   = "roles/resourcemanager.folderAdmin"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_project_creator" {
  org_id = var.organizations
  role   = "roles/resourcemanager.projectCreator"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_project_deleter" {
  org_id = var.organizations
  role   = "roles/resourcemanager.projectDeleter"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_project_iam_admin" {
  org_id = var.organizations
  role   = "roles/resourcemanager.projectIamAdmin"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_project_mover" {
  org_id = var.organizations
  role   = "roles/resourcemanager.projectMover"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_billing_user" {
  org_id = var.organizations
  role   = "roles/billing.user"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_compute_admin" {
  org_id = var.organizations
  role   = "roles/compute.admin"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
resource "google_organization_iam_member" "service_account_git_binding_compute_xpnAdmin" {
  org_id = var.organizations
  role   = "roles/compute.xpnAdmin"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}

resource "google_organization_iam_member" "service_account_git_binding_organizationAdmin" {
  org_id = var.organizations
  role   = "roles/resourcemanager.organizationAdmin"
  member = "serviceAccount:${google_service_account.service_account_git.email}"
}
