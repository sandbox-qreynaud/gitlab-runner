output "sa_automation_email" {
    value       = google_service_account.service_account_git.email
    description = "Generated email address of the Gitlab runner service account"
}

output "runner_instance_self_link" {
    value       = google_compute_instance.runner.self_link
    description = "Self link of the gitlab runner instance"
}