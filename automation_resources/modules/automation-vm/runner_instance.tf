/***************************************************
  Book external static ip
****************************************************/
resource "google_compute_address" "static-ip" {
  name   = "gitlab-runner-ip"
  region = var.region
}

/***************************************************
  Gitlab-runner instance configuration
****************************************************/
resource "google_compute_instance" "runner" {
  name         = "gitlab-runner"
  machine_type = var.machine_type
  zone         = var.zone

  tags = ["gitlab-runner"]

  labels = {
    perimerter    = "shared-services"
    role          = "automation"
    origin-date   = substr(timestamp(), 0, 10)
  }


  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
       nat_ip = google_compute_address.static-ip.address
    }
  }


  metadata_startup_script = file("${path.module}/runner-startup-script.sh")

  service_account {
    scopes = ["compute-rw"]
    email  = google_service_account.service_account_git.email
  }
}