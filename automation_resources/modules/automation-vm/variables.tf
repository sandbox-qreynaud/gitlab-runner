
/********************************************
* GITLAB RUNNER INSTANCE
********************************************/
variable "region" {
    type        = string
    default     = "europe-west1"
    description = "The region where the gitlab runner instance is deployed"
}

variable "zone" {
    type        = string
    default     = "europe-west1-b"
    description = "The zone where the gitlab runner instance is deployed"
}

variable "machine_type" {
    type        = string 
    default     = "n1-standard-1"
    description = "GCE machine type of the GitLan runner instance. Adjust to the needed performance"
}

/********************************************
* GITLAB RUNNER SERVICE ACCOUNT
********************************************/
variable "service_account_id" {
    type       = string
    default    = "gcp_sa_shared_services_runner"
    decription = "ID used in the generated service account email address"
}

variable "service_account_name" {
    type        = string
    default     = "gitlab-runner"
    description = "Name of the service account" 
}



