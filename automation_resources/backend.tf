terraform {
  backend "gcs" {
    bucket  = "qrd-automation-cicd-tfstate"
    prefix  = "gitlab_runner/terraform/state"
  }
}