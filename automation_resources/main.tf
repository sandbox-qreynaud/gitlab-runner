### AUTOMATION GITLAB RUNNER ###
################################

module "gitlab-runner-instance" {
    source       = "./modules/automation-vm"
    machine_type = "g1-small"
}

module "enable-api" {
    source       = "./modules/automation-api"
    project_id   = "ss-automation"
}