terraform {
  backend "gcs" {
    bucket  = "ss-automation-cicd-60255-tf-state"
    prefix  = "gitlab_runner/terraform/state"
  }
}

