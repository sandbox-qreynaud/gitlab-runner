provider "google" {
  project     = "sandbox-qreynaud"
  region      = "europe-west1"
}

resource "google_compute_instance" "default" {
  name         = "created-by-gitlab-runner"
  machine_type = "f1-micro"
  zone         = "europe-west1-b"

  tags = ["create-by-gitlabci"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}


